# README #

This is the repo for the Psychosis Engine.
All code is free to use as long as use of the engine is credited.

Feel free to fork the repo and make whatever improvements or adjustments you wish.
My plans are to eventually replace the usage of GLM to a built in library but that will come in the future.