#pragma once

#ifdef __APPLE__
    #include "glm.hpp"
    #include <SDL2/SDL.h>
#elif _WIN32
    #include <glm\glm.hpp>
    #include <SDL/SDL.h>
#endif

#include <unordered_map>
#include "Vector.h"

namespace Psychosis{
	class InputManager
	{
	public:
		InputManager();
		~InputManager();

		SDL_GameController *gameContoller;

		void update();

		void pressKey(unsigned int keyID);
		void releaseKey(unsigned int keyID);
		void setMouseCoords(float x, float y);
		//True If Key is Held Down
		bool isKeyDown(unsigned int keyID);
		//True If Key is Pressed on Frame
		bool isKeyPressed(unsigned int keyID);
		int getKey();
		char getCharacter();

		glm::vec2 getMouseCoords() const{
			return _mouseCoords;
		}

		Vector2D getLeftStick() { return m_joyLeft; }
		Vector2D getRightStick() { return m_joyRight; }

		void setJoyLeftX(float pos) { m_joyLeft.x = pos; };
		void setJoyLeftY(float pos) { m_joyLeft.y = pos; };
		void setJoyRightX(float pos) { m_joyRight.x = pos; };
		void setJoyRightY(float pos) { m_joyRight.y = pos; };

		void pressButton(unsigned int keyID);
		void releaseButton(unsigned int keyID);
		bool isButtonDown(unsigned int keyID);
		bool isButtonPressed(unsigned int keyID);


	private:
		std::unordered_map<unsigned int, bool> _keyMap;
		std::unordered_map<unsigned int, bool> _prevKeyMap;
		glm::vec2 _mouseCoords;

		std::unordered_map<unsigned int, bool> m_joyButtons;
		std::unordered_map<unsigned int, bool> m_prevJoyButtons;
		Vector2D m_joyLeft, m_joyRight;

		bool wasKeyDown(unsigned int keyID);
		bool wasButtonDown(unsigned int keyID);
	};
}