#pragma once

#ifdef __APPLE__
    #include <SDL2_mixer/SDL_mixer.h>
#elif _WIN32
    #include <SDL\SDL_mixer.h>
#endif

#include <string>
#include <map>

namespace Psychosis{
	class SoundEffect{
	public:
		friend class AudioEngine;

		void play(int loops = 0, int channel = 0);
	private:
		Mix_Chunk* m_chunk = nullptr;
	
	};

	class Music{
	public:
		friend class AudioEngine;

		void play(int loops = -1);

		static void pause();
		static void stop();
		static void resume();

	private:
		Mix_Music* m_music = nullptr;
	};

	class AudioEngine {
	public:
		AudioEngine();
		~AudioEngine();

		void init();
		void destroy();

		void changeVolume(int volume);

		SoundEffect loadSoundEffect(const std::string& filePath);
		Music loadMusic(const std::string& filePath);

	private:
		std::map < std::string, Mix_Chunk* > m_effectMap;
		std::map <std::string, Mix_Music*> m_musicMap;

		bool m_isInitialised = false;
	};

}