#include "Errors.h"
#include <assert.h>

#ifdef __APPLE__
    #include "CharIO.h"
    #include <SDL2/SDL.h>
#elif _WIN32
    #include <conio.h>
    #include <SDL\SDL.h>
#endif

#include <iostream>
#include <cstdlib>
namespace Psychosis{
	void fatalError(std::string errorString){
		std::cout << errorString << std::endl;
		std::printf("Enter a key to exit!");
		_getch();
		system("pause");
		SDL_Quit();
		exit(1);
	}
}