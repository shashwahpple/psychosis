#pragma once
#include"GL_Texture.h"
#include<string>
namespace Psychosis{
	class ImageLoader{

	public:
		static GL_Texture loadPNG(std::string filePath);
	};
}
