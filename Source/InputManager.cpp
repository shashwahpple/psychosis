#include "InputManager.h"

namespace Psychosis{
	InputManager::InputManager() : _mouseCoords(0)
	{
	}


	InputManager::~InputManager()
	{
	}

	void InputManager::update(){
		//Loop through Keymap with For Each Loop
		for (auto& it : _keyMap){
			_prevKeyMap[it.first] = it.second;
		}

		for (auto& it : m_joyButtons){
			m_prevJoyButtons[it.first] = it.second;
		}
	}

	char SDLKeyToChar(int key) {
		switch (key) {
		case SDLK_TAB:          return char(0x9);
		case SDLK_RETURN:       return '\n';
		case SDLK_SPACE:        return ' ';
		case SDLK_COMMA:        return ',';
		case SDLK_MINUS:        return '-';
		case SDLK_PERIOD:       return '.';
		case SDLK_SLASH:        return '/';
		case SDLK_0:            return '0';
		case SDLK_1:            return '1';
		case SDLK_2:            return '2';
		case SDLK_3:            return '3';
		case SDLK_4:            return '4';
		case SDLK_5:            return '5';
		case SDLK_6:            return '6';
		case SDLK_7:            return '7';
		case SDLK_8:            return '8';
		case SDLK_9:            return '9';
		case SDLK_COLON:        return ':';
		case SDLK_SEMICOLON:    return ';';
		case SDLK_EQUALS:       return '=';
		case SDLK_LEFTBRACKET:  return '[';
		case SDLK_BACKSLASH:    return '\\';
		case SDLK_RIGHTBRACKET: return ']';
		case SDLK_a:            return 'a';
		case SDLK_b:            return 'b';
		case SDLK_c:            return 'c';
		case SDLK_d:            return 'd';
		case SDLK_e:            return 'e';
		case SDLK_f:            return 'f';
		case SDLK_g:            return 'g';
		case SDLK_h:            return 'h';
		case SDLK_i:            return 'i';
		case SDLK_j:            return 'j';
		case SDLK_k:            return 'k';
		case SDLK_l:            return 'l';
		case SDLK_m:            return 'm';
		case SDLK_n:            return 'n';
		case SDLK_o:            return 'o';
		case SDLK_p:            return 'p';
		case SDLK_q:            return 'q';
		case SDLK_r:            return 'r';
		case SDLK_s:            return 's';
		case SDLK_t:            return 't';
		case SDLK_u:            return 'u';
		case SDLK_v:            return 'v';
		case SDLK_w:            return 'w';
		case SDLK_x:            return 'x';
		case SDLK_y:            return 'y';
		case SDLK_z:            return 'z';
		case SDLK_KP_0:         return '0';
		case SDLK_KP_1:         return '1';
		case SDLK_KP_2:         return '2';
		case SDLK_KP_3:         return '3';
		case SDLK_KP_4:         return '4';
		case SDLK_KP_5:         return '5';
		case SDLK_KP_6:         return '6';
		case SDLK_KP_7:         return '7';
		case SDLK_KP_8:         return '8';
		case SDLK_KP_9:         return '9';
		case SDLK_KP_PERIOD:    return '.';
		case SDLK_KP_DIVIDE:    return '/';
		case SDLK_KP_MULTIPLY:  return '*';
		case SDLK_KP_MINUS:     return '-';
		case SDLK_KP_PLUS:      return '+';
		case SDLK_KP_ENTER:     return '\n';
		case SDLK_KP_EQUALS:    return '=';
		default:                return -1;
		}
	}

	char InputManager::getCharacter() {
		return SDLKeyToChar(getKey());
	}

	int InputManager::getKey() {
		auto it = _keyMap.find(true);
		if (it != _keyMap.end()) {
			return it->first;
		} 

		return -1;
	}

	void InputManager::pressKey(unsigned int keyID){
		_keyMap[keyID] = true;
	}

	void InputManager::releaseKey(unsigned int keyID){
		_keyMap[keyID] = false;
	}

	void InputManager::setMouseCoords(float x, float y){
		_mouseCoords.x = x;
		_mouseCoords.y = y;
	}

	bool InputManager::isKeyDown(unsigned int keyID){
		auto it = _keyMap.find(keyID);
		if (it != _keyMap.end()){
			return it->second;
		}
		else {
			return false;
		}
	}

	bool InputManager::isKeyPressed(unsigned int keyID){
		if (isKeyDown(keyID) == true && !wasKeyDown(keyID)){
			return true;
		}
		return false;
	}

	bool InputManager::wasKeyDown(unsigned int keyID){
		auto it = _prevKeyMap.find(keyID);
		if (it != _prevKeyMap.end()){
			return it->second;
		}
		else {
			return false;
		}
	}

	void InputManager::pressButton(unsigned int keyID){
		m_joyButtons[keyID] = true;
	}

	void InputManager::releaseButton(unsigned int keyID){
		m_joyButtons[keyID] = false;
	}

	bool InputManager::isButtonDown(unsigned int keyID){
		auto it = m_joyButtons.find(keyID);
		if (it != m_joyButtons.end()){
			return it->second;
		}
		else {
			return false;
		}
	}

	bool InputManager::isButtonPressed(unsigned int keyID){
		if (isButtonDown(keyID) == true && !wasButtonDown(keyID)){
			return true;
		}
		return false;
	}

	bool InputManager::wasButtonDown(unsigned int keyID){
		auto it = m_prevJoyButtons.find(keyID);
		if (it != m_prevJoyButtons.end()){
			return it->second;
		}
		else {
			return false;
		}
	}

}