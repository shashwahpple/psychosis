#include"IOManager.h"
#include<fstream>
namespace Psychosis{
	bool IOManager::readFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer){
		std::ifstream file(filePath, std::ios::binary);
		if (file.fail()){
			perror(filePath.c_str());
			return false;
		}

		//Seek to end
		file.seekg(0, std::ios::end);
		//Get file size
		int fileSize = (int)file.tellg();

		file.seekg(0, std::ios::beg);
		//Reduce file size by header bytes
		fileSize -= file.tellg();

		buffer.resize(fileSize);
		file.read((char *)&(buffer[0]), fileSize);
		file.close();
		return true;
	}

	bool IOManager::readFileToBuffer(std::string filePath, std::string& buffer){
		std::ifstream file(filePath, std::ios::binary);
		if (file.fail()){
			perror(filePath.c_str());
			return false;
		}

		//Seek to end
		file.seekg(0, std::ios::end);
		//Get file size
		int fileSize = (int)file.tellg();

		file.seekg(0, std::ios::beg);
		//Reduce file size by header bytes
		fileSize -= file.tellg();

		buffer.resize(fileSize);
		file.read((char *)&(buffer[0]), fileSize);
		file.close();
		return true;
	}

}