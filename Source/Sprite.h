#pragma once

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif _WIN32
    #include <GL\glew.h>
#endif

#include "GL_Texture.h"
#include <string>
namespace Psychosis{
	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		void init(float x, float y, float width, float height, std::string texturePath);

		void draw();

	private:
		float _x;
		float _y;
		float _width;
		float _height;
		GLuint _vboID;
		GL_Texture _texture;
	};
}