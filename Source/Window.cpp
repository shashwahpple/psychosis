#include "Window.h"
#include "Errors.h"
namespace Psychosis{
	Window::Window()
	{
	}


	Window::~Window()
	{
	}

	int Window::create(std::string windowName, int screenWidth, int screenHeight, unsigned int currentFlags){
		Uint32 flags = SDL_WINDOW_OPENGL;

		if (currentFlags & INVISIBLE){
			flags |= SDL_WINDOW_HIDDEN;
		}

		if (currentFlags & FULLSCREEN){
			flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		if (currentFlags & BORDERLESS){
			flags |= SDL_WINDOW_BORDERLESS;
		}
        
		_sdlWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);
		//Error checking
		if (_sdlWindow == nullptr){
			fatalError("SDL Window could not be created. :(");
		}

		SDL_GLContext glContext = SDL_GL_CreateContext(_sdlWindow);
		if (glContext == nullptr){
			fatalError("OpenGl Context could not be created. :(");
		}

#ifdef _WIN32
		GLenum error = glewInit();
		if (error != GLEW_OK){
			fatalError("Could not initialise Glew. :(");
		}
#endif
		//Check OpenGL Version
		std::printf("***  OpenGL Version: %s  ***\n", glGetString(GL_VERSION));

		glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		//Set VSync to True
		SDL_GL_SetSwapInterval(1);

        glOrtho(0, 0, screenWidth, screenHeight, -1, 1);
        
		//Enable Alpha Blending
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		return 0;
	}

	void Window::swapBuffer(){
		SDL_GL_SwapWindow(_sdlWindow);
	}
}