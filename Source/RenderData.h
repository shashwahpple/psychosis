#ifndef __PSYCHOSIS_RENDER_DATA__
#define __PSYCHOSIS_RENDER_DATA__

#include "SpriteBatch.h"
#include "Vector.h"
#include "ResourceManager.h"

#define RAD_DEG_RATIO 0.0174533f
#define DEFAULT_ANGLE 0.0f

namespace Psychosis {

	class RenderData {
	public:
		RenderData(Vector2D pos, Vector2D size, std::string filePath, float depth, ColorRGBA8 color = ColorRGBA8(255, 255, 255, 255));
		~RenderData();

		void Render(SpriteBatch& spriteBatch);

		Vector2D GetPosition() { return Vector2D(m_destRect.x, m_destRect.y); }
		void SetPosition(Vector2D& pos) { m_destRect.x = pos.x; m_destRect.y = pos.y; }

		Vector2D GetSize() { return Vector2D(m_destRect.z, m_destRect.w); }
		void SetSize(Vector2D& size) { m_destRect.z = size.x; m_destRect.w = size.y; }

		float GetDepth() { return m_depth; }
		void SetDepth(float depth) { m_depth = depth; }

		float GetAngle() { return m_angle; }
		void SetAngle(float angle) { m_angle = angle; }

		ColorRGBA8 GetColor() { return m_imageColor; }
		void SetColor(ColorRGBA8& color) { m_imageColor = color; }

		std::string GetPath() { return m_filePath; }
		void SetPath(std::string& path) { m_filePath = path; m_id = ResourceManager::getTexture(path).id; }

		Vector4D GetUV() { return m_uvRect; }
		void SetUV(Vector4D& uv) { m_uvRect = uv; }

	protected:
		ColorRGBA8 m_imageColor;
		Vector4D m_destRect, m_uvRect;
		GLuint m_id;
		float m_depth, m_angle;
		std::string m_filePath;
	};

}
#endif