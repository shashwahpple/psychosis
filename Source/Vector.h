#pragma once

#ifdef __APPLE__
    #include "glm.hpp"
#elif _WIN32
    #include <glm/glm.hpp>
#endif

namespace Psychosis{
	struct Vector2D{
		float x;
		float y;

		Vector2D() : x(0), y(0) {}
		Vector2D(float i) : x(i), y(i) {}
		Vector2D(float x, float y) : x(x), y(y) {}

		Vector2D operator + (Vector2D& v) { return Vector2D(x + v.x, y + v.y); }
		Vector2D operator - (Vector2D& v) { return Vector2D(x - v.x, y - v.y); }
		Vector2D operator * (Vector2D& v) { return Vector2D(x * v.x, y * v.y); }
		Vector2D operator / (Vector2D& v) { return Vector2D(x / v.x, y / v.y); }
        Vector2D operator + (int i) { return Vector2D(x + i, y + i); }
		Vector2D operator / (float v) { return Vector2D(x / v, y / v); }
        Vector2D operator / (int v) { return Vector2D(x / v, y / v); }
        Vector2D operator * (float i) { return Vector2D(x * i, y * i); }
		Vector2D operator -() { return Vector2D(-x, -y); }
	};

	struct IVector2D {
		int x;
		int y;

		IVector2D() : x(0), y(0) {}
		IVector2D(int i) : x(i), y(i) {}
		IVector2D(int x, int y) : x(x), y(y) {}

		IVector2D operator + (IVector2D& v) { return IVector2D(x + v.x, y + v.y); }
		IVector2D operator - (IVector2D& v) { return IVector2D(x - v.x, y - v.y); }
		IVector2D operator * (IVector2D& v) { return IVector2D(x * v.x, y * v.y); }
		IVector2D operator / (IVector2D& v) { return IVector2D(x / v.x, y / v.y); }
		IVector2D operator / (float v) { return IVector2D(int(x / v), int(y / v)); }
	};

	struct Vector4D {
		float x, y, z, w;

		Vector4D() : x(0), y(0), z(0), w(0) {}
		Vector4D(float i) : x(i), y(i), z(i), w(i) {}
		Vector4D(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}
		Vector4D(Vector2D& v) : x(0), y(0), z(v.x), w(v.y) {}

	};

	class Vector{
	public:
		Vector();
		~Vector();

		static float Deg2Rad(float d);
		static float Rad2Deg(float r);
		static float Angle(Vector2D& v);
		static float Distance(float x, float y);
		static Vector2D Distance(Vector2D& v, Vector2D& b);
		static Vector2D Normalize(Vector2D& v);
		static Vector2D Rotate(Vector2D& v, float a);
		static glm::vec2 Rotate(glm::vec2& v, float a);
		static Vector4D Rotate(Vector4D& v, float rot);
		static glm::vec2 Vec2Iso(glm::vec2& v);
		static glm::vec2 IsoVec2(glm::vec2& v);
	};

}