#pragma once

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif _WIN32
    #include <GL\glew.h>
#endif

namespace Psychosis{
	struct GL_Texture {
		GLuint id;
		int width;
		int height;
	};
}