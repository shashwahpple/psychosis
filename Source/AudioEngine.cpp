#include "AudioEngine.h"
#include "Errors.h"

namespace Psychosis{
	void SoundEffect::play(int loops, int channel){
		if (Mix_PlayChannel(-1, m_chunk, loops) == -1){
			if (Mix_PlayChannel(channel, m_chunk, loops) == -1){
				fatalError("Mix_PlayChannel error: " + std::string(Mix_GetError()));
			}
		}
	}

	void Music::play(int loops){
		Mix_PlayMusic(m_music, loops);
	}

	void AudioEngine::changeVolume(int volume){
		//Mix_VolumeMusic(volume);
		Mix_Volume(-1, volume);
	}

	void Music::pause(){
		Mix_PauseMusic();
	}
	void Music::stop(){
		Mix_HaltMusic();
	}
	void Music::resume(){
		Mix_ResumeMusic();
	}

	AudioEngine::AudioEngine()
	{
		//EMPTY
	}


	AudioEngine::~AudioEngine()
	{
		destroy();
	}

	void AudioEngine::init(){
		if (m_isInitialised){
			fatalError("Tried to initialise AudioEngine twice!");
		}


		// Param can be Bitwise of MIX_INIT_FAC,
		// MIX_INIT_MOD, MIX_INIT_MP3, MIX_INIT_OGG
		if (Mix_Init(MIX_INIT_OGG) == -1){
			fatalError("Mix_Init error: " + std::string(Mix_GetError()));
		}
		if (Mix_Init(MIX_INIT_MP3) == -1){
			fatalError("Mix_Init error: " + std::string(Mix_GetError()));
		}
		if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1){
			fatalError("Mix_OpenAudio error: " + std::string(Mix_GetError()));
		}

		m_isInitialised = true;
	}
	void AudioEngine::destroy(){
		if (m_isInitialised){
			m_isInitialised = false;

			for (auto& it : m_effectMap){
				Mix_FreeChunk(it.second);
			}

			for (auto& it : m_musicMap){
				Mix_FreeMusic(it.second);
			}
			m_effectMap.clear();
			m_musicMap.clear();

			Mix_CloseAudio();
			Mix_Quit();
		}
	}

	SoundEffect AudioEngine::loadSoundEffect(const std::string& filePath){
		auto it = m_effectMap.find(filePath);
		
		SoundEffect effect;

		if (it == m_effectMap.end()){
			Mix_Chunk* chunk = Mix_LoadWAV(filePath.c_str());
			if (chunk == nullptr){
				fatalError("Mix_LoadWAV error: " + std::string(Mix_GetError()));
			}
			effect.m_chunk = chunk;
			m_effectMap[filePath] = chunk;
		}
		else{
			effect.m_chunk = it->second;
		}

		return effect;
	}
	Music AudioEngine::loadMusic(const std::string& filePath){
		auto it = m_musicMap.find(filePath);

		Music music;

		if (it == m_musicMap.end()){
			Mix_Music* mix_music = Mix_LoadMUS(filePath.c_str());
			if (mix_music == nullptr){
				fatalError("Mix_LoadMUS error: " + std::string(Mix_GetError()));
			}
			music.m_music = mix_music;
			m_musicMap[filePath] = mix_music;
		}
		else{
			music.m_music = it->second;
		}

		return music;
	}
}