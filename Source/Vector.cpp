#include "Vector.h"
#include <math.h>

#define Pi 3.14195265358979323846

namespace Psychosis{
	Vector::Vector() {

	}


	Vector::~Vector() {

	}

	float Vector::Distance(float x, float y){
		return (sqrt(y * y) - sqrt(x * x));
	}

	Vector2D Vector::Distance(Vector2D& v, Vector2D& b){
		return Vector2D(sqrt((b.x - v.x) * (b.x - v.y)), sqrt((b.y - v.y) * (b.y - v.y)));
	}

	Vector2D Vector::Normalize(Vector2D& v){
		float len = sqrt((v.x * v.x) + (v.y * v.y));
		return (Vector2D(v.x / len, v.y / len));
	}

	float Vector::Deg2Rad(float d) {
		return d * (float)(Pi / 180);
	}

	float Vector::Rad2Deg(float r) {
		return r * (float)(180 / Pi);
	}

	/// Rotates a Vector2D using Degrees as the supplied angle.
	Vector2D Vector::Rotate(Vector2D& v, float a) {
		float r = Deg2Rad(a);
		float px = v.x * std::cos(r) - v.y * std::sin(r);
		float py = v.x * std::sin(r) + v.y * std::cos(r);
		return Vector2D(px, py);
	}
	
	glm::vec2 Vector::Rotate(glm::vec2& v, float a) {
		float r = Deg2Rad(a);
		float px = v.x * std::cos(r) - v.y * std::sin(r);
		float py = v.x * std::sin(r) + v.y * std::cos(r);
		return glm::vec2(px, py);
	}

	Vector4D Vector::Rotate(Vector4D& v, float rot){
		float cs = cos(rot);
		float sn = sin(rot);

		float px = v.x * cs - v.y * sn;
		float py = v.x * sn + v.y * cs;

        Vector2D dist = Distance(*new Vector2D(v.x, v.y), *new Vector2D(v.z, v.w));

		float pz = dist.x * cs - dist.y * sn;
		float pw = dist.x * sn + dist.y * cs;

		return Vector4D(px, py, pz, pw);
	}

	glm::vec2 Vector::Vec2Iso(glm::vec2 & v) {
		return glm::vec2(v.x - v.y, (v.x + v.y) / 2);
	}

	glm::vec2 Vector::IsoVec2(glm::vec2 & v) {
		return glm::vec2((2 * v.y + v.x) / 2, (2 * v.y - v.x) / 2);
	}

	float Vector::Angle(Vector2D& v) {
		return atan2(v.y, v.x);
	}
}