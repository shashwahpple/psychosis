#include "TextureCache.h"
#include "ImageLoader.h"
#include <iostream>
namespace Psychosis{
	TextureCache::TextureCache(){

	}


	TextureCache::~TextureCache(){

	}

	GL_Texture Psychosis::TextureCache::getTexture(std::string texturePath){
		auto mit = _textureMap.find(texturePath);

		if (mit == _textureMap.end()){
			GL_Texture newTexture = ImageLoader::loadPNG(texturePath);
			_textureMap.insert(make_pair(texturePath, newTexture));

			return newTexture;
		}

		return mit->second;
	}
}