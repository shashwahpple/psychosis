#pragma once

#ifdef __APPLE__
    #include <SDL2/SDL.h>
    #include <OpenGL/glu.h>
#elif _WIN32
    #include <SDL\SDL.h>
    #include <GL\glew.h>
#endif

#include <string>
namespace Psychosis{
	enum WindowFlags {
		NONE = 0x0,
		INVISIBLE = 0x1,
		FULLSCREEN = 0x2,
		BORDERLESS = 0x4
	};

	class Window
	{
	public:
		Window();
		~Window();

		int create(std::string windowName, int screenWidth, int screenHeight, unsigned int currentFlags);

		void swapBuffer();

		int getScreenWidth() { return _screenWidth; }
		int getScreenHeight() { return _screenHeight; }
	private:
		SDL_Window* _sdlWindow;
		int _screenWidth;
		int _screenHeight;
	};
}