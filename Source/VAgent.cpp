#include "VAgent.h"

namespace Psychosis {

	VAgent::VAgent() {
		m_direction = glm::vec2(1.0f, 0.0f);
	}


	VAgent::~VAgent() {

	}

	ColData VAgent::CollideCircles(int index) {
		ColData dat = { false, 0.0f, glm::vec2(0.0f) };
		VAgent* agent = m_agentsVector->at(index);
		float colDepth = glm::distance(m_position, agent->GetPosition());

		if (colDepth < (m_radius + agent->GetRadius())) {
			dat.collision = true;
			dat.depth = colDepth / 2.0f;
			dat.direction = glm::normalize(m_position - agent->GetPosition());
		}

		return dat;
	}

	ColData VAgent::CollideRects(int index) {
		ColData dat = { false, 0.0f, glm::vec2(0.0f) };
		VAgent* agent = m_agentsVector->at(index);

		glm::vec2 tl, tr, bl, br;
		glm::vec2 atl, atr, abl, abr;
		glm::vec2 ap = agent->GetPosition(), as = agent->GetSize();

		bl = m_position; br = glm::vec2(m_position.x + m_size.x, m_position.y); tl = glm::vec2(m_position.x, m_position.y + m_size.y); tr = m_position + m_size;
		abl = ap; abr = glm::vec2(ap.x + as.x, ap.y); atl = glm::vec2(ap.x, ap.y + as.y); atr = ap + as;

		if (bl.x > abl.x && bl.x < abr.x && bl.y > abl.y && bl.y < tl.y) {
			dat.collision = true;
		}
		else if (br.x > abl.x && br.x < abr.x && br.y > abl.y && br.y < atl.y) {
			dat.collision = true;
		}
		else if (tl.x > abl.x && tl.x < abr.x && tl.y > abl.y && tl.y < atl.y) {
			dat.collision = true;
		}
		else if (tr.x > abl.x && tr.x < abr.x && tr.y > abl.y && tr.y < atl.y) {
			dat.collision = true;
		}

		return dat;
	}

	void VAgent::Draw(SpriteBatch& spriteBatch) {
		const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

		glm::vec4 destRect;

		destRect.x = m_position.x;
		destRect.y = m_position.y;
		destRect.z = m_size.x;
		destRect.w = m_size.y;

		spriteBatch.draw(destRect, uvRect, m_textureId, m_depth, ColorRGBA8(255, 255, 255, 255), m_direction);
	}

}