#include "RenderData.h"

namespace Psychosis {

	RenderData::RenderData(Vector2D pos, Vector2D size, std::string filePath, float depth, ColorRGBA8 color) {
		///	Sets the Destination Rectangle to the Position as XY and the Size as ZW.
		m_destRect.x = pos.x; m_destRect.y = pos.y;
		m_destRect.z = size.x; m_destRect.w = size.y;

		///	Sets all the attached member variables to the supplied parameters.
		m_filePath = filePath;
		m_depth = depth;
		m_imageColor = color;
		m_angle = DEFAULT_ANGLE;

		m_uvRect = Vector4D(0.0f, 0.0f, 1.0f, 1.0f);

		m_id = ResourceManager::getTexture(filePath).id;
	}

	RenderData::~RenderData() {

	}

	void RenderData::Render(SpriteBatch& spriteBatch) {
		const glm::vec4 uvRect = glm::vec4(m_uvRect.x, m_uvRect.y, m_uvRect.z, m_uvRect.w);

		///	Creates a glm::vec4 version of the Destination Rectangle of the RenderData
		glm::vec4 destRect;
		destRect.x = m_destRect.x; destRect.y = m_destRect.y;
		destRect.z = m_destRect.z; destRect.w = m_destRect.w;

		///	Draws the supplied GLuint onto the screen. This GLuint is just a long int of the supplied Texture.
		spriteBatch.draw(destRect, uvRect, m_id, m_depth, m_imageColor, m_angle);
	}

}