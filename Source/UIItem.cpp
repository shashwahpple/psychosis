#include "UIItem.h"

namespace PsyUI{
	UIItem::UIItem() : m_depth(0.0f), m_angle(0.0f){
        m_color = Psychosis::ColorRGBA8(255, 255, 255, 255);
    }

	UIItem::UIItem(Psychosis::Vector2D& position, std::string filePath, Psychosis::Vector2D size){
		m_colBox.x = position.x; m_colBox.y = position.y;
		m_textureID = Psychosis::ResourceManager::getTexture(filePath).id;
		m_color = Psychosis::ColorRGBA8(255, 255, 255, 255);
		m_colBox.z = size.x; m_colBox.w = size.y;
		m_uvRect = Psychosis::Vector4D(0.0f, 0.0f, 1.0f, 1.0f);
		setAngle(0.0f);
	}


	UIItem::~UIItem(){
	
	}

	void UIItem::render(Psychosis::SpriteBatch& spriteBatch){
		const glm::vec4 uvRect = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

		glm::vec4 destRect;
		destRect.x = m_colBox.x;
		destRect.y = m_colBox.y;
		destRect.z = m_colBox.z;
		destRect.w = m_colBox.w;

        if (m_angle != NULL){
            spriteBatch.draw(destRect, uvRect, m_textureID, m_depth, m_color, m_angle);
        } else {
            spriteBatch.draw(destRect, uvRect, m_textureID, m_depth, m_color, m_dir);
        }
	}

	void UIItem::Update(float deltaTime) {
		OnUpdate(deltaTime);
	}

	void UIItem::Collide(glm::vec2& position) {
		if (position.x > m_colBox.x && position.x < m_colBox.z) {
			if (position.y > m_colBox.y && position.y < m_colBox.w) {
				OnCollide(position);
			}
		}
	}
}