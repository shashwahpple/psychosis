#pragma once
#include "TextureCache.h"
#include <string>
namespace Psychosis{
	class ResourceManager{
	public:
		static GL_Texture getTexture(std::string texturePath);
	private:
		static TextureCache _textureCache;
	};
}
