#include "IGameScreen.h"
#include "Psychosis.h"
#include <iostream>
#include <string>
#include <random>
#include <ctime>
#include <algorithm>

namespace Psychosis{
	IGameScreen::IGameScreen(){

	}


	IGameScreen::~IGameScreen(){

	}

	void IGameScreen::initScreen(Psychosis::IScreenHandler& handler, Psychosis::InputManager& inputManager,
		Psychosis::GLSL_Program& textureProgram){
		m_parentHandler = &handler;
		m_inputManager = &inputManager;
		m_textureProgram = &textureProgram;

		initExtras();
	}
}