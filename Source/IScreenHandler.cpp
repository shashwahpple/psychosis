#include "IScreenHandler.h"
#include "Psychosis.h"
#include "IGameScreen.h"
#include <iostream>
#include <string>
#include <random>
#include <ctime>
#include <algorithm>

namespace Psychosis{
	IScreenHandler::IScreenHandler(){

	}

	void IScreenHandler::loadScreenSettings(){
		std::ifstream ifs("Saves/screenSettings.sav");
		if (ifs >> m_screenSettings){
			m_screenSize = m_screenSettings.screenSize;
			if (m_screenSettings.borderless){
				m_screenFlags |= WindowFlags::BORDERLESS;
			}
			if (m_screenSettings.fullscreen){
				m_screenFlags |= WindowFlags::FULLSCREEN;
			}
			if (m_screenSettings.invisible){
				m_screenFlags |= WindowFlags::INVISIBLE;
			}

		}
		else {
			m_screenSettings.borderless = false;
			m_screenSettings.fullscreen = false;
			m_screenSettings.invisible = false;
			m_screenSettings.screenSize = m_screenSize;
            saveScreenSettings();
		}
	}

	void IScreenHandler::saveScreenSettings(){
		std::ofstream ofs("Saves/screenSettings.sav");
		ofs << m_screenSettings;
		ofs.close();
	}

	IScreenHandler::~IScreenHandler(){
		for (size_t i = 0; i < m_gameScreens.size(); i++) {
			delete m_gameScreens[i];
		}
	}

	void IScreenHandler::nextScreen(){
		if (m_currentScreen + 1 >= (uint8_t)m_gameScreens.size()){
			m_currentScreen = 0;
		} else {
			m_currentScreen++;
		}
	}

	void IScreenHandler::prevScreen(){
		if (m_currentScreen - 1 < 0){
			m_currentScreen = (uint8_t)m_gameScreens.size() - 1;
		}
		else {
			m_currentScreen--;
		}
	}

	void IScreenHandler::run(){
		initSystems();

		while (m_appState != AppState::EXIT){
			loop();
		}
	}

	void IScreenHandler::shutdown() {
		m_appState = AppState::EXIT;
	}

	void IScreenHandler::initSystems(){
		Psychosis::init();

		m_window.create(m_windowName, (int)m_screenSize.x, (int)m_screenSize.y, m_screenFlags);
		glClearColor(0.5f, 0.3f, 0.3f, 1.0f);

		initShaders();
		initScreens();

		m_fpsLimiter.init(m_fps.x);
		m_audioEngine.init();

		if (SDL_NumJoysticks > 0){
			m_controller = SDL_GameControllerOpen(0);
		}

		m_appState = AppState::PLAYING;
	}

	void IScreenHandler::initShaders(){
#ifdef __APPLE__
        m_textureProgram.compileShaders("Shaders/colorShading_OSX.vert", "Shaders/colorShading_OSX.frag");
#elif _WIN32
		m_textureProgram.compileShaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
#endif
		m_textureProgram.addAttribute("vertexPosition");
		m_textureProgram.addAttribute("vertexColor");
		m_textureProgram.addAttribute("vertexUV");
		m_textureProgram.linkShaders();
	}

	void IScreenHandler::loop(){
		const float MS_PER_SECOND = 1000.0f;
		const float DESIRED_FRAMETIME = MS_PER_SECOND / m_fps.x;
		const float MAX_DELTA_TIME = 1.0f;

		float previousTicks = (float)SDL_GetTicks();

		while (m_appState == AppState::PLAYING){
			const int MAX_PHYSICS_STEPS = 6;

			m_fpsLimiter.begin();

			float newTicks = (float)SDL_GetTicks();
			float frameTime = newTicks - previousTicks;
			previousTicks = newTicks;
			float totalDeltaTime = frameTime / DESIRED_FRAMETIME;

			getInput();

			int i = 0;
			while (totalDeltaTime > 0.0f && i < MAX_PHYSICS_STEPS){
				m_delta = std::min(totalDeltaTime, MAX_DELTA_TIME);

				if (m_inputCountdown > 0.0f){
					m_inputCountdown -= 1.0f * m_delta;
				}

				m_gameScreens[m_currentScreen]->update(m_delta);

				totalDeltaTime -= m_delta;

				i++;
			}

			draw();
			m_gameScreens[m_currentScreen]->updateCamera();
		}
	}
	
	float IScreenHandler::getDelta() {
		return m_delta;
	}

	void IScreenHandler::draw(){
		glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_textureProgram.use();

		glActiveTexture(GL_TEXTURE0);
		GLuint textureUniform = m_textureProgram.getUniformLocation("mySampler");
		glUniform1i(textureUniform, 0);

		m_gameScreens[m_currentScreen]->draw();

		m_textureProgram.unuse();

		m_window.swapBuffer();
	}
	
	void IScreenHandler::getInput(){
		SDL_Event evnt;

		while (SDL_PollEvent(&evnt)) {
			switch (evnt.type) {
			case SDL_QUIT:
				m_appState = AppState::EXIT;
				break;
			case SDL_MOUSEMOTION:
				m_inputManager.setMouseCoords((float)evnt.motion.x, (float)evnt.motion.y);
				break;
			case SDL_KEYDOWN:
				m_inputManager.pressKey(evnt.key.keysym.sym);
				break;
			case SDL_KEYUP:
				m_inputManager.releaseKey(evnt.key.keysym.sym);
				break;
			case SDL_MOUSEBUTTONDOWN:
				m_inputManager.pressKey(evnt.button.button);
				break;
			case SDL_MOUSEBUTTONUP:
				m_inputManager.releaseKey(evnt.button.button);
				break;
			case SDL_CONTROLLERAXISMOTION:
				switch (evnt.caxis.axis){
				case 0:
					m_inputManager.setJoyLeftX((float)evnt.caxis.value);
					break;
				case 1:
					m_inputManager.setJoyLeftY((float)-evnt.caxis.value);
					break;
				case 2:
					m_inputManager.setJoyRightX((float)evnt.caxis.value);
					break;
				case 3:
					m_inputManager.setJoyRightY((float)-evnt.caxis.value);
					break;
				}
				break;
			case SDL_CONTROLLERBUTTONDOWN:
				m_inputManager.pressButton(evnt.cbutton.button);
				break;
			case SDL_CONTROLLERBUTTONUP:
				m_inputManager.releaseButton(evnt.cbutton.button);
				break;
			}
		}
	}

}