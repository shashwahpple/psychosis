#include "GLSL_Program.h"
#include "Errors.h"
#include <fstream>
#include <vector>
#include "IOManager.h"

namespace Psychosis{
	GLSL_Program::GLSL_Program() : m_numAttributes(0), m_programID(0), m_vertexShaderID(0), m_fragmentShaderID(0) {

	}


	GLSL_Program::~GLSL_Program() {

	}

	void GLSL_Program::compileShaders(const std::string& filePathVertex, const std::string& filePathFragment) {

		std::string fragSource;
		std::string vertSource;

		IOManager::readFileToBuffer(filePathVertex, vertSource);
		IOManager::readFileToBuffer(filePathFragment, fragSource);

		compileShadersFromSource(vertSource.c_str(), fragSource.c_str());

	}

	void GLSL_Program::compileShadersFromSource(const char* vertexSource, const char* fragmentSource){

		m_programID = glCreateProgram();
		m_vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		if (m_vertexShaderID == 0){
			fatalError("Vertex shader failed to be created. :(");
		}

		m_fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		if (m_fragmentShaderID == 0){
			fatalError("Fragment shader failed to be created. :(");
		}

		compileShader(vertexSource, "Vertex Shader", m_vertexShaderID);
		compileShader(fragmentSource, "Fragment Shader", m_fragmentShaderID);
	}

	void GLSL_Program::linkShaders() {
		glAttachShader(m_programID, m_vertexShaderID);
		glAttachShader(m_programID, m_fragmentShaderID);

		glLinkProgram(m_programID);

		GLuint isLinked = 0;
		glGetProgramiv(m_programID, GL_LINK_STATUS, (int *)&isLinked);
		if (isLinked == GL_FALSE){
			GLint maxLength = 0;
			glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(m_programID, maxLength, &maxLength, &infoLog[0]);

			glDeleteProgram(m_programID);
			glDeleteShader(m_vertexShaderID);
			glDeleteShader(m_fragmentShaderID);

			std::printf("%s\n", &(infoLog[0]));
			fatalError("Shader(s) failed to link. :(");
		}

		glDetachShader(m_programID, m_vertexShaderID);
		glDetachShader(m_programID, m_fragmentShaderID);
	}

	void GLSL_Program::compileShader(const char* source, const std::string& name, GLuint id){

		glShaderSource(id, 1, &source, nullptr);
		glCompileShader(id);

		GLint success = 0;
		glGetShaderiv(id, GL_COMPILE_STATUS, &success);

		if (success == GL_FALSE){
			GLint maxLength = 0;
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<char> errorLog(maxLength);
			glGetShaderInfoLog(id, maxLength, &maxLength, &errorLog[0]);

			glDeleteShader(id);

			std::printf("%s\n", &(errorLog[0]));
			fatalError("Shader " + name + " failed to compile. :(");
		}
	}

	void GLSL_Program::addAttribute(const std::string& attributeName){
		glBindAttribLocation(m_programID, m_numAttributes++, attributeName.c_str());
	}

	GLuint GLSL_Program::getUniformLocation(const std::string uniformName){
		GLint location = glGetUniformLocation(m_programID, uniformName.c_str());
#ifdef __APPLE__
    #define GL_INVALID_INDEX 4294967295
#endif
        if (location == GL_INVALID_INDEX){
			fatalError("Uniform " + uniformName + " not found in shader. :(");
		}
		return location;
	}

	void GLSL_Program::use(){
		glUseProgram(m_programID);
		for (int i = 0; i < m_numAttributes; i++){
			glEnableVertexAttribArray(i);
		}
	}

	void GLSL_Program::unuse(){
		glUseProgram(0);
		for (int i = 0; i < m_numAttributes; i++){
			glDisableVertexAttribArray(i);
		}
	}
}