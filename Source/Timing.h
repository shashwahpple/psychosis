#pragma once

#ifdef __APPLE__
    #include <SDL2/SDL.h>
#elif _WIN32
    #include <SDL\SDL.h>
#endif

namespace Psychosis{
	class FpsLimiter{
	public:
		FpsLimiter();
		void init(float maxFps);
		void setMaxFps(float maxFps);

		void begin();

		float end();

	private:
		void calculateFps();
		float _maxFps;
		float _fps;
		float _frameTime;
		unsigned int _startTicks;
	};

}