#pragma once
#include <map>
#include "GL_Texture.h"
namespace Psychosis{
	class TextureCache{
	public:
		TextureCache();
		~TextureCache();
		GL_Texture getTexture(std::string texturePath);

	private:
		std::map<std::string, GL_Texture> _textureMap;
	};
}