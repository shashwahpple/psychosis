#pragma once
#include <string>

namespace Psychosis{
	extern void fatalError(std::string errorString);
}