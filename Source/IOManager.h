#pragma once
#include<vector>
namespace Psychosis{
	class IOManager{

	public:
		static bool readFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer);
		static bool readFileToBuffer(std::string filePath, std::string& buffer);

		bool saveFileFromBuffer(std::string filePath, std::vector<unsigned char>& buffer);
	};
}
