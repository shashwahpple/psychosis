#pragma once
#include <vector>
#include <string>

#include <glm\glm.hpp>
#include "GL_Texture.h"
#include "SpriteBatch.h"
#include "ResourceManager.h"

namespace Psychosis {

	struct ColData {
		bool collision;
		float depth;
		glm::vec2 direction;
	};

	class VAgent {
	public:
		VAgent();
		~VAgent();

		void init(std::vector<VAgent*>* agentsVector, std::string& texturePath) { m_agentsVector = agentsVector; m_textureId = ResourceManager::getTexture(texturePath).id; }

		virtual void update(float deltaTime) = 0;

		float GetRadius() { return m_radius; }
		glm::vec2 GetPosition() { return m_position; }
		void SetPosition(const glm::vec2 pos) { m_position = pos; }
		glm::vec2 GetSize() { return m_size; }
		void SetSize(glm::vec2 size) { m_size = size; }
		glm::vec2 GetDirection() { return m_direction; }
		void SetDirection(glm::vec2 direction) { m_direction = direction; }
		glm::vec2 GetVelocity() { return m_velocity; }
		void SetVelocity(glm::vec2 velocity) { m_velocity = velocity; }

		void Draw(SpriteBatch& spriteBatch);

	protected:
		ColData CollideCircles(int index);
		ColData CollideRects(int index);
		ColData CollideCircRect(int index) {}

		std::vector<VAgent*>* m_agentsVector;
		glm::vec2 m_position, m_direction, m_velocity;

		float m_radius, m_depth = 0.0f;
		glm::vec2 m_size;
		GLuint m_textureId;
	};

}