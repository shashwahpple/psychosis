#pragma once

#include "InputManager.h"
#include "IScreenHandler.h"
#include "GLSL_Program.h"

const int JOYSTICK_DEAD_ZONE = 3000;

namespace Psychosis{

	class IGameScreen
	{
	public:
		IGameScreen();
		~IGameScreen();

		void initScreen(IScreenHandler& handler, InputManager& inputManager, GLSL_Program& textureProgram);

		virtual void update(float deltaTime) = 0;
		virtual void draw() = 0;
		virtual void updateCamera() = 0;

	protected:
		GLSL_Program* m_textureProgram;
		IScreenHandler* m_parentHandler;
		InputManager* m_inputManager;

		virtual void initExtras() = 0;
	};
}