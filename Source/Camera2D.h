#pragma once

#ifdef __APPLE__
    #include "glm.hpp"
    #include "matrix_transform.hpp"
#elif _WIN32
    #include <glm\glm.hpp>
    #include <glm\gtc\matrix_transform.hpp>
	#include "Vector.h"
#endif

namespace Psychosis{
	class Camera2D
	{
	public:
		Camera2D();
		~Camera2D();

		void init(int screenWidth, int screenHeight);

		void update();

		glm::vec2 convertScreenToWorld(glm::vec2 screenCoords);

		bool isBoxInView(const glm::vec2& position, const glm::vec2 dimensions);
		bool isBoxInView(const Vector2D& position, const Vector2D& dimensions) {return isBoxInView(glm::vec2(position.x, position.y), glm::vec2(dimensions.x, dimensions.y)); }

		//Setters
		void setPosition(const glm::vec2& newPosition){
			m_needsMatrixUpdate = true;
			m_position = newPosition;
		}
		void setScale(float newScale){
			m_needsMatrixUpdate = true;
			m_scale = newScale;
		}

		//Getters
		glm::vec2 getPosition() {
			return m_position;
		}
		float getScale(){
			return m_scale;
		}
		glm::mat4 getOrthoMatrix(){
			return m_cameraMatrix;
		}

	private:
		int m_screenWidth;
		int m_screenHeight;
		bool m_needsMatrixUpdate;
		float m_scale;
		glm::vec2 m_position;
		glm::mat4 m_cameraMatrix;
		glm::mat4 m_orthoMatrix;
	};
}