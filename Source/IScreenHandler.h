#pragma once
#include <fstream>
#include <iostream>
#include <bitset>
#include <vector>
#include <string>
#include "Window.h"
#include "Vector.h"
#include "GLSL_Program.h"
#include "InputManager.h"
#include "Timing.h"
#include "AudioEngine.h"

namespace Psychosis{
	class ScreenSettings{
	public:
		bool invisible, fullscreen, borderless;
		Vector2D screenSize;

		friend std::ostream& operator<<(std::ostream& os, const ScreenSettings& s)
		{
			// write out individual members of s with an end of line between each one
			os << s.invisible << '\n';
			os << s.borderless << '\n';
			os << s.fullscreen << '\n';
			os << s.screenSize.x << '\n';
			os << s.screenSize.y;
			return os;
		}

		friend std::istream& operator>>(std::istream& is, ScreenSettings& s)
		{
			// read in individual members of s
			is >> s.invisible >> s.borderless >> s.fullscreen >> s.screenSize.x >> s.screenSize.y;
			return is;
		}

	};

	typedef std::bitset<8> Byte;
	enum class AppState{
		EXIT = 0x0,
		RUNNING = 0x1,
		PLAYING = 0x2
	};

	class IGameScreen;

	class IScreenHandler {
	public:
		IScreenHandler();
		~IScreenHandler();

		void run();

		Vector2D getScreenSize() { return m_screenSize; }

		void nextScreen();
		void prevScreen();
		void setScreen(int n) { m_currentScreen = n; }

		void setInputCountdown(float delay) { m_inputCountdown = delay; }
		float getInputCountdown() { return m_inputCountdown; }

		AppState m_appState;
		ScreenSettings m_screenSettings;
		void loadScreenSettings();
		void saveScreenSettings();
		float getDelta();
		float getFps() { return m_fps.y; }
		AudioEngine& getAudioEngine() { return m_audioEngine; }
		void shutdown();

	protected:
		SDL_GameController* m_controller = NULL;

		unsigned int m_screenFlags;
		uint8_t m_currentScreen = 0x0;
		std::vector<IGameScreen*> m_gameScreens;
		float m_inputCountdown = 0.0f;

		Window m_window;
		std::string m_windowName;
		Vector2D m_screenSize, m_fps;
		FpsLimiter m_fpsLimiter;
		InputManager m_inputManager;
		GLSL_Program m_textureProgram;
		AudioEngine m_audioEngine;

		void initSystems();
		void initShaders();
		virtual void initScreens() = 0;

		void loop();
		void draw();
		void getInput();
		float m_delta;
	};
}