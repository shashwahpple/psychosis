#include "Timing.h"

namespace Psychosis{
	FpsLimiter::FpsLimiter(){
	
	}

	void FpsLimiter::init(float maxFps){
		setMaxFps(maxFps);
	}

	void FpsLimiter::setMaxFps(float maxFps){
		_maxFps = maxFps;
	}

	void FpsLimiter::begin(){
		_startTicks = SDL_GetTicks();
	}

	float FpsLimiter::end(){
		calculateFps();

		float frameTicks = (float)SDL_GetTicks() - (float)_startTicks;
		//Limit FPS to maximum
		if (1000.0f / _maxFps > frameTicks){
			SDL_Delay(Uint32(1000.0f / _maxFps - frameTicks));
		}

		return _fps;

	}

	void FpsLimiter::calculateFps(){
		//Creating frame counter
		static const int NUM_SAMPLES = 10;
		static int currentFrame = 0;
		static float frameTimes[NUM_SAMPLES];
		//Setting prevTicks to amount of ticks past since SDL_Init()
		static float prevTicks = (float)SDL_GetTicks();
		float currentTicks;
		//Set currentTicks then _frameTime
		currentTicks = (float)SDL_GetTicks();
		_frameTime = currentTicks - prevTicks;
		prevTicks = (float)SDL_GetTicks();
		//Setting frameTimes to the _frameTime
		frameTimes[currentFrame % NUM_SAMPLES] = _frameTime;
		int count;
		currentFrame++;
		if (currentFrame < NUM_SAMPLES){
			count = currentFrame;
		}
		else{
			count = NUM_SAMPLES;
		}
		float frameTimeAvg = 0;

		for (int i = 0; i < count; i++){
			frameTimeAvg += frameTimes[i];
		}
		frameTimeAvg /= count;

		if (frameTimeAvg > 0){
			_fps = 1000.0f / frameTimeAvg;
		}
		else{
			_fps = 60;
		}
	}

}