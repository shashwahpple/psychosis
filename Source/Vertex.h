#pragma once

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif _WIN32
    #include <GL\glew.h>
#endif

namespace Psychosis{
	struct ColorRGBA8 {
		ColorRGBA8() : R(0), G(0), B(0), A(0){}
		ColorRGBA8(GLubyte r, GLubyte g, GLubyte b, GLubyte a) :
			R(r), G(g), B(b), A(a){}
		GLubyte R;
		GLubyte G;
		GLubyte B;
		GLubyte A;
	};

	struct Position {
		float X;
		float Y;
	};

	struct UV{
		float U;
		float V;
	};

	struct Vertex {
		Position position;
		ColorRGBA8 color;
		UV uv;

		void setPosition(float x, float y){
			position.X = x;
			position.Y = y;
		}

		void setColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a){
			color.R = r;
			color.G = g;
			color.B = b;
			color.A = a;
		}

		void setUV(float u, float v){
			uv.U = u;
			uv.V = v;
		}
	};
}