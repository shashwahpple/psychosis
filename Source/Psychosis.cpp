#ifdef __APPLE__
    #include <SDL2/SDL.h>
    #include <OpenGL/glu.h>
#elif _WIN32
    #include <SDL\SDL.h>
    #include <GL\glew.h>
#endif

#include "Psychosis.h"

namespace Psychosis{
	int init(){
		//Initialise SDL
		SDL_Init(SDL_INIT_EVERYTHING);

		//Set double buffer
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}
}