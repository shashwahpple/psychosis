#pragma once
#include "SpriteBatch.h"
#include "Vector.h"
#include "GL_Texture.h"
#include "SpriteBatch.h"
#include "ResourceManager.h"
#include <string>

namespace PsyUI{
	class UIItem {
	public:
		UIItem();
		UIItem(Psychosis::Vector2D& position, std::string filePath, Psychosis::Vector2D size);
		~UIItem();

		void render(Psychosis::SpriteBatch& spriteBatch);

		float getAngle() { return m_angle; }

		Psychosis::Vector2D getDimensions() { return Psychosis::Vector2D(m_colBox.z, m_colBox.w); }

		Psychosis::Vector2D getPosition() { return Psychosis::Vector2D(m_colBox.x, m_colBox.y); }
		void setPosition(Psychosis::Vector2D v){
			m_colBox.x = v.x; m_colBox.y = v.y; }

		void setDepth(float depth) { m_depth = depth; }

		void setAngle(float angle) { m_angle = angle; Psychosis::Vector::Rotate(m_colBox, angle); }

        void setAngle(glm::vec2 d) { m_dir = d;}

		void setInfo(std::string n, Psychosis::Vector2D s, float d) { 
			m_textureID = Psychosis::ResourceManager::getTexture(n).id;
			m_colBox.z = s.x; m_colBox.w = s.y;
			m_depth = d;
		}
		
		void Update(float deltaTime);
		void Collide(glm::vec2& position);

	protected:
        glm::vec2 m_dir;
		float m_angle;
		Psychosis::Vector4D m_colBox;
		Psychosis::Vector4D m_uvRect;
		Psychosis::ColorRGBA8 m_color;
		GLuint m_textureID;
		float m_depth;

		virtual void OnUpdate(float deltaTime) {}
		virtual void OnCollide(glm::vec2& position) {}
	};
}
