#pragma once

#ifdef __APPLE__
    #include <OpenGL/glu.h>
#elif _WIN32
    #include <GL\glew.h>
#endif

#include <string>
namespace Psychosis{
	class GLSL_Program{
	public:
		GLSL_Program();
		~GLSL_Program();

		void compileShadersFromSource(const char* vertexSource, const char* fragmentSource);
		void compileShaders(const std::string& filePathVertex, const std::string& filePathFragment);
		void linkShaders();

		void use();
		void unuse();

		GLuint getUniformLocation(const std::string uniformName);

		void addAttribute(const std::string& attributeName);
	private:
		int m_numAttributes;

		void compileShader(const char* source, const std::string& name, GLuint id);
		
		GLuint m_programID;
		GLuint m_vertexShaderID;
		GLuint m_fragmentShaderID;
	};
}
